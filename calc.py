def sumar(x, y):
    return x + y


def restar(x, y):
    return y - x


if __name__ == '__main__':
    print(sumar(1, 2))
    print(sumar(3, 4))
    print(restar(5, 6))
    print(restar(7, 8))
